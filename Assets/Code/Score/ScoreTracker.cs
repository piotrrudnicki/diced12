using System;

namespace Code.Score
{
    public class ScoreTracker
    {
        private int totalScore;
        
        public event Action<int> OnNewScoreAdded;
        public event Action<int> OnTotalScoreChanged;
        public void AddNewScore(int score)
        {
            OnNewScoreAdded?.Invoke(score);
            totalScore += score;
            OnTotalScoreChanged?.Invoke(totalScore);
        }
    }
}