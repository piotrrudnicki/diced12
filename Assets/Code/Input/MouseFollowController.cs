using System;
using UnityEngine;

namespace Code.Input
{
    public class MouseFollowController : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        
        private const string DESKTOP_LAYER = "Desktop";
        private int desktopMask;
        private Vector3 newPosition = Vector3.zero;
        
        public bool Follow{ get; set; }
        public event Action<Vector3> OnFollowedPositionChanged;
        private void Awake()
        {
            desktopMask = LayerMask.GetMask(DESKTOP_LAYER);
        }
        
        private void Update()
        {
            if (!mainCamera || !Follow) return;
            var mousePosition = UnityEngine.Input.mousePosition;
            var mouseRay = mainCamera.ScreenPointToRay(mousePosition);
            if (Physics.Raycast(mouseRay, out var hit, Mathf.Infinity, desktopMask))
            {
                newPosition = hit.point;
            }

            OnFollowedPositionChanged?.Invoke(newPosition);
        }
    }
}
