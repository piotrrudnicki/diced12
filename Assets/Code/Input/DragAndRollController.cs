using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Input
{
    public class DragAndRollController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        public event Action OnDragStart;
        public event Action OnDragEnd;
        public event Action <Vector2> OnDragMove;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            OnDragStart?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnDragEnd?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            OnDragMove?.Invoke(eventData.delta);
        }
    }
}
