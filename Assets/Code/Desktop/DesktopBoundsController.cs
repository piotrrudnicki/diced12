using UnityEngine;

namespace Code.Desktop
{
    public class DesktopBoundsController : MonoBehaviour
    {
        public Vector2 XBounds { get; private set; }
        public Vector2 ZBounds { get; private set; }
        
        private void Awake()
        {
            CalculateBounds();
        }
    
        private void CalculateBounds()
        {
            var desktopCollider = GetComponent<BoxCollider>();
            Vector3 colliderCenter = desktopCollider.center;
            Vector3 colliderSize = desktopCollider.size;
            Vector3 worldCenter = transform.TransformPoint(colliderCenter);
            
            Vector3 worldSize = new Vector3(
                colliderSize.x * transform.lossyScale.x,
                colliderSize.y * transform.lossyScale.y,
                colliderSize.z * transform.lossyScale.z
            );
            
            var minimumBounds = worldCenter - worldSize * 0.5f;
            var maximumBounds = worldCenter + worldSize * 0.5f;
            XBounds = new Vector2(minimumBounds.x, maximumBounds.x);
            ZBounds = new Vector2(minimumBounds.z, maximumBounds.z);
        }
    }
}
