using System;
using System.Threading;
using Code.Data;
using Code.Score;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.Dice
{
    public class DiceController : MonoBehaviour
    {
        [SerializeField] private DiceDefinition definition;
        [SerializeField] private DiceSide[] sides;
        [SerializeField] private DiceDragController dragController;
        [SerializeField] private Rigidbody rigidbody;
        [SerializeField] private float torqueForce;
        [SerializeField] private float maxForce;

        private bool isRolling;
        private ScoreTracker scoreTracker;
        private DiceTopSideChecker topSideChecker;
        private DiceRollingService rollingService;
        private DicePositionCorrector positionCorrector;
        private CancellationTokenSource cancellationTokenSource;

        public event Action OnRollStarted;
  
        #region Initialization
        public void Construct(ScoreTracker scoreTracker, Vector2 xBounds, Vector2 zBounds)
        {
            this.scoreTracker = scoreTracker;
            positionCorrector = new DicePositionCorrector(transform, xBounds, zBounds);
        }
        
        private void Awake()
        {
            InitDiceSides();
            InitRollingService();
            SubscribeToEvents();
        }
        
        private void InitDiceSides()
        {
            if (definition.Sides.Length != sides.Length)
            {
                Debug.LogError("Dice sides count does not match definition sides count!");
                return;
            }
            
            for (int i = 0; i < definition.Sides.Length; i++)
            {
                sides[i].Value = definition.Sides[i];
            }
            
            topSideChecker = new DiceTopSideChecker(sides);
        }

        private void InitRollingService()
        {
            cancellationTokenSource = new CancellationTokenSource();
            rollingService = new DiceRollingService(rigidbody, torqueForce, maxForce, cancellationTokenSource);
        }

        private void SubscribeToEvents()
        {
            dragController.OnDiceRolled += Roll;
            rollingService.OnRollFinished += CheckResults;
        }
        #endregion Initialization
        
        private void CheckResults()
        {
            scoreTracker.AddNewScore(topSideChecker.GetSideValue());
            dragController.CanDrag = true;
            isRolling = false;
        }

        public void RandomRoll()
        {
            if (isRolling) return;
            transform.position = Vector3.up * 3f;
            Roll(Random.insideUnitCircle * maxForce);
        }
        
        private async void Roll(Vector2 force)
        {
            isRolling = true;
            dragController.CanDrag = false;
            OnRollStarted?.Invoke();
            await rollingService.Roll(force);
        }

        private void FixedUpdate()
        {
            positionCorrector.CorrectPosition();
        }
        
        private void OnDestroy()
        {
            cancellationTokenSource.Cancel();
        }
    }
}
