using System;
using Code.Input;
using UnityEngine;

namespace Code.Dice
{
    public class DiceDragController : MonoBehaviour
    {
        [SerializeField] private DragAndRollController dragAndRoll;
        [SerializeField] private MouseFollowController mouseFollowController;
        [SerializeField] private Rigidbody rigidbody;
        [SerializeField] private float dragHeight = 3f;
    
        private bool isDragging;
        private Vector2 dragDelta;
        public bool CanDrag { get; set; } = true;
        public event Action<Vector2> OnDiceRolled;

        private void Awake()
        {
            dragAndRoll.OnDragStart += OnDragStart;
            dragAndRoll.OnDragEnd += OnDragEnd;
            dragAndRoll.OnDragMove += OnDragMove;
            mouseFollowController.OnFollowedPositionChanged += OnMousePositionChanged;
        }
    
        private void OnDragStart()
        {
            if (!CanDrag) return;
            dragDelta = Vector2.zero;
            rigidbody.isKinematic = true;
            rigidbody.velocity = Vector3.zero;
            transform.position += Vector3.up * dragHeight;
            mouseFollowController.Follow = true;
        }
    
        private void OnDragEnd()
        {
            if (!CanDrag) return;
            mouseFollowController.Follow = false;
            rigidbody.isKinematic = false;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            if(dragDelta.sqrMagnitude < 20f)
            {
                return;
            }
            
            OnDiceRolled?.Invoke(dragDelta);
        }
    
        private void OnDragMove(Vector2 delta)
        {
            if (!CanDrag) return;
            dragDelta = delta;
        }

        private void OnMousePositionChanged(Vector3 pos)
        {
            pos.y = dragHeight;
            transform.position = pos;
        }
    }
}
