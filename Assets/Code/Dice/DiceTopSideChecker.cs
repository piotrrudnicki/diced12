using UnityEngine;

namespace Code.Dice
{
    public class DiceTopSideChecker
    {
        private readonly DiceSide[] sides;
        
        public DiceTopSideChecker(DiceSide[] sides)
        {
            this.sides = sides;
        }
        
        public int GetSideValue()
        {
            float closestDot = -Mathf.Infinity;
            int topFaceIndex = -1;
            for (int i = 0; i < sides.Length; i++)
            {
                float dot = sides[i].UpDotProduct;
                if (!(dot > closestDot)) continue;
                closestDot = dot;
                topFaceIndex = i;
            }
            return sides[topFaceIndex].Value;
        }
    }
}