using UnityEngine;

namespace Code.Dice
{
    public class DicePositionCorrector
    {
        private readonly Transform transform;
        private readonly Vector2 xBounds;
        private readonly Vector2 zBounds;
        
        public DicePositionCorrector(Transform transform, Vector2 xBounds, Vector2 zBounds)
        {
            this.transform = transform;
            this.xBounds = xBounds;
            this.zBounds = zBounds;
        }
        
        public void CorrectPosition()
        {
            var pos = transform.position;
            pos.x = Mathf.Clamp(pos.x, xBounds.x, xBounds.y);
            pos.z = Mathf.Clamp(pos.z, zBounds.x, zBounds.y);
            transform.position = pos;
        }
    }
}