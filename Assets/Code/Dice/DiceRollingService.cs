using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Code.Dice
{
    public class DiceRollingService
    {
        private Rigidbody rigidbody;
        private float maxForce;
        private float torqueForce;
        private readonly CancellationTokenSource cancellationTokenSource;

        public DiceRollingService(Rigidbody rigidbody, float maxForce, float torqueForce, CancellationTokenSource cancellationTokenSource)
        {
            this.rigidbody = rigidbody;
            this.maxForce = maxForce;
            this.torqueForce = torqueForce;
            this.cancellationTokenSource = cancellationTokenSource;
        }
        
        public event Action OnRollFinished;
        public async Task Roll(Vector2 force)
        {
            var x = Mathf.Clamp(force.x, -maxForce, maxForce);
            var z = Mathf.Clamp(force.y, -maxForce, maxForce);
            var newForce = new Vector3(x,0,z);
            rigidbody.AddForce(newForce, ForceMode.Impulse);
            rigidbody.AddTorque(new Vector3(z,-x, 0) * torqueForce, ForceMode.Impulse);
            await WaitForResult();
        }

        private async Task WaitForResult()
        {
            await Task.Delay(1000);
            while (rigidbody.velocity.sqrMagnitude > float.Epsilon)
            {
                await Task.Yield();
            }

            if (cancellationTokenSource.IsCancellationRequested) return;

            OnRollFinished?.Invoke();
        }
    }
}