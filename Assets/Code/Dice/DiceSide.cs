using TMPro;
using UnityEngine;

namespace Code.Dice
{
    public class DiceSide : MonoBehaviour
    {
        [SerializeField] private TextMeshPro sideText;
        private int sideValue;
        
        public int Value
        {
            get => sideValue;
            set
            {
                sideValue = value;
                SetSideText(value);
            }
        }
        public float UpDotProduct => Vector3.Dot(transform.up, Vector3.up);

        private void SetSideText(int value)
        {
            sideText.SetText(value.ToString());
        }
    }
}
