using Code.Dice;
using Code.Score;
using TMPro;
using UnityEngine;

namespace Code.UI
{
    public class ResultValueDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI resultText;
        
        public void Construct(ScoreTracker scoreTracker, DiceController diceController)
        {
            scoreTracker.OnNewScoreAdded += UpdateValue;
            diceController.OnRollStarted += ClearValue;
        }
        
        private void UpdateValue(int value)
        {
            resultText.SetText($"RESULT: {value}");
        }

        private void ClearValue()
        {
            resultText.SetText("RESULT: ?");
        }
        
    }
}
