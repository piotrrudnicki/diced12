using Code.Score;
using TMPro;
using UnityEngine;

namespace Code.UI
{
    public class TotalScoreDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI totalScoreText;
    
        public void Construct(ScoreTracker scoreTracker)
        {
            scoreTracker.OnTotalScoreChanged += UpdateTotalScore;
        }

        private void UpdateTotalScore(int totalScore)
        {
            totalScoreText.SetText($"TOTAL SCORE: {totalScore}");
        }
    }
}
