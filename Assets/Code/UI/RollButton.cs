using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code.UI
{
    public class RollButton : MonoBehaviour
    {
        [SerializeField] private Button button;

        public event Action OnRollButtonClicked;

        private void Awake()
        {
            button.onClick.AddListener(OnRollButtonClickedHandler);
        }
        
        private void OnRollButtonClickedHandler()
        {
            OnRollButtonClicked?.Invoke();
        }
    }
}
