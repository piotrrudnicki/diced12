using UnityEngine;

namespace Code.Data
{
    [CreateAssetMenu(fileName = "DiceDefinition", menuName = "Data/DiceDefinition", order = 0)]
    public class DiceDefinition : ScriptableObject
    {
        [SerializeField] private int[] sides;
        
        public int[] Sides => sides;
    }
}
