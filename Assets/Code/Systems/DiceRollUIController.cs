using Code.Dice;
using Code.Score;
using Code.UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Code.Systems
{
    public class DiceRollUIController : MonoBehaviour
    {
        [FormerlySerializedAs("currentValueDisplay")] [SerializeField] private ResultValueDisplay resultValueDisplay;
        [SerializeField] private TotalScoreDisplay totalScoreDisplay;
        [SerializeField] private RollButton rollButton;
        
        public void Construct(ScoreTracker scoreTracker, DiceController diceController)
        {
            resultValueDisplay.Construct(scoreTracker, diceController);
            totalScoreDisplay.Construct(scoreTracker);
            rollButton.OnRollButtonClicked += diceController.RandomRoll;
        }
    }
}