using Code.Desktop;
using Code.Dice;
using Code.Score;
using UnityEngine;

namespace Code.Systems
{
    public class DiceRollGameController : MonoBehaviour
    {
        [SerializeField] private DiceController diceController;
        [SerializeField] private DiceRollUIController uiController;
        [SerializeField] private DesktopBoundsController boundsController;
        
        private ScoreTracker scoreTracker;
        
        private void Awake()
        {
            scoreTracker = new ScoreTracker();
            uiController.Construct(scoreTracker, diceController);
            diceController.Construct(scoreTracker, boundsController.XBounds, boundsController.ZBounds);
        }
    }
}